<?php





# 	Configuración predeterminada

$minutos_cache = 60;

$formulario_activo = true;
$color_activo = true;
$menu_activo = true;
$tabla_activo = true;



$fuente_normal        = 'Ubuntu-L';
$fuente_elegante      = 'Comfortaa-Light';
$fuente_monoespaciada = 'UbuntuMono-R';


// Colores

$transparente = '#fff0';

$blanco = '#fff';
$negro  = '#000';

$gris = '#777';
$gris_claro  = '#999';
$gris_oscuro = '#555';

$azul = '#35a';
$azul_claro = '#cdf';

$verde = '#295';
$verde_claro = '#dfd';

$lila = '#95d';
$lila_claro = '#ece';

$rojo = '#d33';
$rojo_claro = '#fcc';

$amarillo = '#c85';
$amarillo_claro = '#ffd';



$css_comun_relleno = '1em 1.5em';
$css_comun_margen  = '0.2em';

$css_boton_relleno = '1em 1.5em';
$css_boton_margen  = '0.2em';

$css_texto_relleno = '0.5em 2em';
$css_texto_margen  = '0.2em';

$css_formulario_campo_relleno = '0.5em';
$css_formulario_campo_margen  = '0.2em';

$css_h_relleno = '.2em 0px';
$css_h_margen  = '0px';

$css_encabezado_margen = '0px';
$css_encabezado_relleno= '1em 2em';

$css_seccion_margen = '0px';
$css_seccion_relleno= '3vw 5vw';

$css_bloque_margen = '3vw 1vw';
$css_bloque_relleno= '1vw';

$css_articulo_margen = '2vw 5vw';
$css_articulo_relleno= '2vw 5vw';

$css_panel_interior_margen = '0px';
$css_panel_interior_relleno= '1em 2em';

$css_pie_margen = 'auto';
$css_pie_relleno= '5vw';

$css_pie_p_margen = '1vw';
$css_pie_p_relleno= 'auto';


$css_menu_nav_a_margen = '.2em';
$css_menu_nav_a_relleno= '.8em 2em';
;

#	Configuración personalizada
require_once ('esquema/configuracion.php');



header('Content-type: text/css');
$segundos_memoria = $minutos_cache * 60;
$vencimiento = gmdate("D, d M Y H:i:s", time() + $segundos_memoria) . " GMT";

header("Expires: $vencimiento");
header("Cache-Control: max-age=$segundos_memoria");






require_once ('esquema/funciones.php');


?>

/*
	Esquema PHP

	Por Javier Martínez
	
	Versión: 22.10

	Estilo con lineamientos genéricos para el modelado de páginas con diseño responsable.



	Fuentes:

		Ubuntu: http://font.ubuntu.com/

		Comfortaa: http://aajohan.deviantart.com/



	Basado en:

		siimple
			https://www.siimple.xyz/

		milligram
			https://milligram.io/

		Flusk
			https://github.com/technext/Flusk

*/

<?php

/* <?=$variable ?> */



// -------------------------- //
























?>







<?php







css_fuente ($fuente_elegante);
css_fuente ($fuente_monoespaciada);
css_fuente ($fuente_normal);
# TODO usar fuentes en las clases




?>






/*
	Elementos
*/



html { scroll-behavior:smooth; }

html {
  font-family: '<?=$fuente_elegante ?>', '<?=$fuente_normal ?>', 'roboto';
  text-rendering: optimizeLegibility;
  max-width: 100%;
}



body {
  margin : 0px;
  padding: 0px;
  letter-spacing: .01em;
}




input [disabled] {
  cursor: default;
  opacity: .5;
}


<?=css ("
*       { box-sizing: border-box;     }
*       { transition: all 2s ease;    }
*:hover { transition: all 0.2s ease;  }
") ?>






/*
	Clases
*/



<?=css ("
.redondeado:not(.conjunto),
.conjunto.redondeado * {
	border-radius: 0.5em;
}


.fijo { position: fixed;}
.absoluto { position: absolute;}

.derecho   { right:  1em;}
.superior  { top:    1em;}
.izquierdo { left:   1em;}
.inferior  { bottom: 1em;}

.centrado_unico > *,
.izquierda:not(.posicion)    { text-align: left; }
.derecha:not(.posicion)      { text-align: right;}
.conjunto.centrado *,
.centrado_unico,
.centrado     { text-align: center;	}
.centrado_unico > * {
	margin-left : auto;
	margin-right: auto;
}

.relleno { padding: 1em;}
.margen  { margin : 1em;}




.conjunto.grande *,
.grande  { font-size: large;  }
.conjunto.xgrande *,
.xgrande { font-size: x-large;}
.conjunto.xxgrande *,
.xxgrande { font-size: xx-large;}
.conjunto.xxxgrande *,
.xxxgrande { font-size: xxx-large;}

.conjunto.mini *,
.mini  { font-size: small;  }
.conjunto.xmini *,
.xmini { font-size: x-small;}
.conjunto.xxmini *,
.xxmini { font-size: xx-small;}
.conjunto.xxxmini *,
.xxxmini { font-size: xxx-small;}

.ancho { width: 100%;}

") ?>





<?php 

	require_once ('esquema/color.php') ;

	// echo css_color (nombre, color, mismo color claro, color letra (botones), color borde (botones));
	echo css_color ('verde', $verde, $verde_claro, $blanco);
	echo css_color ('rojo', $rojo, $rojo_claro, $blanco);
	echo css_color ('amarillo', $amarillo, $amarillo_claro, $blanco);
	echo css_color ('lila', $lila, $lila_claro, $blanco);
	echo css_color ('azul', $azul, $azul_claro, $blanco);
	echo css_color ('blanco', $blanco, $gris_claro, $negro);
	echo css_color ('negro', $negro, $gris_claro, $blanco);
	echo css_color ('transparente', $transparente, $gris, $gris_oscuro, $gris_oscuro);
	echo css_color ('transparente_blanco', $transparente, $gris_claro, $blanco, $blanco);
	echo css_color ('transparente_negro', $transparente, $gris_oscuro, $negro, $negro);

	echo css_degradado ('degradado_azul_oscuro', '45deg', '#000022 10%', '#000099 90%');
	echo css_degradado ('transparencia_azul_lila', '-45deg', '#95ba 10%', '#05da 90%');
	 
?>






<?php require_once ('esquema/cuerpo.php'); ?>

<?php require_once ('esquema/tabla.php'); ?>


<?php require_once ('esquema/tabla.php'); ?>



/*
	Fondo
*/

<?=css ("

.fondofijo {

	background-size: 'cover');

	background:transparent no-repeat center center fixed;
}

") ?>













/*
  Enlaces
*/

<?=css ("

a.enlace:not(.boton),
.conjunto.enlace a:not(.boton) {
		font-weight:bold;
		text-decoration: none;
		background-image: linear-gradient(currentColor, currentColor);
		background-position: 50% 100%;
		background-repeat: no-repeat;
		background-size: 0% 0px;
		transition: background-size 0.5s;
		color: inherit;
}

.conjunto.enlace a:not(.boton):hover,
.conjunto.enlace a:not(.boton):focus,
a.enlace:not(.boton):hover,
a.enlace:not(.boton):focus {
    background-size: 100% 2px;
}

") ?>






<?php require_once ('esquema/menu.php'); ?>


<?php require_once ('esquema/imagen.php'); ?>




<?php require_once ('esquema/formulario.php'); ?>

<?php require_once ('esquema/boton.php'); ?>

<?php require_once ('esquema/texto.php'); ?>



<?php require_once ('esquema/animacion.php'); ?>

<?php require_once ('esquema/notificacion.php'); ?>



<?php require_once ('esquema/movil.php'); ?>