<?php


function css_fuente ($nombre, $extension = 'ttf') {
	echo "
	@font-face {
	 font-family: " . $nombre . ";
	 src:url(\"esquema/fuentes/" . $nombre . "." . $extension . "\") format(\"truetype\");
	}
	";
}







function css_color ($nombre, $color, $color_claro, $color_letra, $color_borde = '') {
    if ($color_borde == '')
        $color_borde = $color;

	$cadena = "
    .{{nombre}}:not(.fondo):not(.conjunto) {
		color: {{color}};
		border-color: {{color_borde}};
		background-color: {{color_claro}};
	}

	.conjunto.boton.{{nombre}} a:not(.boton),
	.boton.{{nombre}}:not(.conjunto),
	.menu nav.{{nombre}} a,
	.fondo.{{nombre}} {
	  border-color: {{color_borde}};
	  background-color: {{color}};
	  color: {{color_letra}};
	}
	";


	$busqueda  = array ('{{nombre}}', '{{color}}', '{{color_claro}}', '{{color_letra}}', '{{color_borde}}');
	$reemplazo = array ($nombre, $color, $color_claro, $color_letra, $color_borde);

	return str_replace ($busqueda, $reemplazo, $cadena);

}


function css_degradado ($nombre, ...$colores) {
    $degradado = implode(', ', $colores);
    
    return '.' . $nombre . ' { ' .
    'background: -webkit-linear-gradient(' . $degradado . ');' . PHP_EOL .
    'background:    -moz-linear-gradient(' . $degradado . ');' . PHP_EOL .
    'background:     -ms-linear-gradient(' . $degradado . ');' . PHP_EOL .
    'background:      -o-linear-gradient(' . $degradado . ');' . PHP_EOL .
    'background:         linear-gradient(' . $degradado . ');' . PHP_EOL .
    '}';
}


const _css_ENCABEZADOS = array (
    '-webkit-',
    '   -moz-',
    '    -ms-',
    '     -o-',
    '        ');



const _css_PROPIEDADES = array (
    'animation',
    'animation-timing-function',
    'animation-fill-mode',
    'animation-delay',
    'appearance',
    'box-shadow',
    'box-sizing',
    'border-radius',
    'transition',
    'transform',
    'background-size'    
);







// FUNCIONES //




$_css_buscar     = array();
$_css_reemplazar = array();

foreach (_css_PROPIEDADES as $propiedad)
    $_css_buscar[] = '/' . $propiedad . '(\s*?):(.*?);/';

foreach (_css_PROPIEDADES as $propiedad) {
    $reemplazo = PHP_EOL;
    foreach (_css_ENCABEZADOS as $encabezado) {
        $reemplazo = $reemplazo . $encabezado . $propiedad . ': $2;' . PHP_EOL;
    }
    $_css_reemplazar[] = $reemplazo;
}



// crea propiedades compatibles con navegadores
function css ($codigo = '') {	
global $_css_buscar;
global $_css_reemplazar;



return preg_replace ($_css_buscar, $_css_reemplazar, $codigo) . PHP_EOL;
}


?>