



/*
	formularios
*/








<?php

echo css ("


.formulario * { font-size: inherit;}



form.formulario,
.formulario form {
	display: block;
	padding: .5em;
	margin: 0px;
	border: solid 1px #ddd;
}




.formulario input:not([type=submit]):not([type=reset]):not([type=button]):not([type=checkbox]),
.formulario select,
.formulario label,
.formulario textarea {
	width: 100%;



}

.formulario input[type='submit'],
.formulario input[type='reset'],
.formulario input[type='button'],
.formulario input.boton {

}


.formulario input,
.formulario select,
.formulario textarea {
	border-style:solid;
	border-width: 1px;
}

.formulario input[type='number'] { padding-right: 0px ;	}


.formulario select:focus,
.formulario input:focus,
.formulario textarea:focus {
	border-color:#7ba;
}











.formulario p,
.formulario label,
.formulario legend {

  margin: 0em 0.5em;
	vertical-align: middle;
}


.formulario legend { padding: .5em 1em;}


.formulario fieldset {
	max-width: 30em;
}






 .formulario input[type=checkbox] {
	  appearance: none;

 background-color: gray;
           border: none;
          display: inline-block;
   vertical-align: middle;
   width : 1.5em;
   height: 1.5em;

 }

.formulario input[type=checkbox]:after {
	transition: all 0.5s;

	content: '\\2714';
	text-align: center;
	font-weight:bold;
	color: transparent;

	display: inline-block;
	width: 1.5em;
	height: 1.5em;

	background-color: #fff;
	border: 1px solid #999;
	
	box-sizing:border-box;
 }

 .formulario input[type=checkbox]:checked:after {
	color: #090;
 }



.formulario input[type=checkbox]:not(.interruptor) {
	
}






.formulario  input[type=checkbox].interruptor {
	
}

.formulario input[type=checkbox].interruptor:checked {
	background-color: #295;
}
.formulario input[type=checkbox].interruptor:after {
	content: ' ';
	
width:  1em;
height: 1.5em;
vertical-align: text-top;



}

.formulario input[type=checkbox].interruptor:checked:after {
	margin-left: 0.5em;
	content: ' ';
	
}




.formulario .columna {
	vertical-align: middle;
}



.formulario.redondeado [type='checkbox']:after,
.formulario.redondeado input,
.formulario.redondeado textarea,
.formulario.redondeado select,
.formulario.redondeado * {
	border-radius: 0.5em;

}

.formulario.grande  * {  font-size: large;	}

") ?>

