
/*
	Menú
*/


<?php
	echo css ("


.menu {
	max-height: 100vh;
	max-width: 100vw;
	overflow-y: auto;
	padding: 1px;
	display: inline-block;
}




.menu nav a  {
	border: solid 1px #abf0;
	text-decoration: none;
	white-space: nowrap;	
	margin : " . $css_menu_nav_a_margen . ";
	padding: " . $css_menu_nav_a_relleno . ";

}


.menu nav a:hover {
  border-color: #55ff;
  filter:brightness(1.2);
}





.menu nav ul {
	list-style: none;
	padding: 0px;
	margin: 0px;
}


.menu nav {
	vertical-align: top;
}



.menu.desplegable:not(:hover) {
	max-width: 3em !important;
	max-height: 3em !important;
	overflow: hidden !important;
	display: inline-block;
	vertical-align: middle;
	background-image: url(esquema/svg/menu.svg)  ;
	background-repeat: no-repeat;
	background-size: 2em;
	background-position: center;
}

.menu.desplegable > *:not(:hover)  {
	opacity: 0;

}

.menu.desplegable {
	overflow: hidden !important;
}

") ?>