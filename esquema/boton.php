



/*
	Diseño de Botones
*/

<?=css ("

.formulario input[type='submit']:not([disabled]),
.boton:not(.conjunto),
.boton a {
  cursor : pointer;
  text-align : center;
  vertical-align : middle;
  text-decoration: none;
	white-space:normal;
	border-style:solid;
	border-width: 1px;
}




.boton:not(.conjunto):hover:not([disabled]),
.formulario input[type='submit']:hover:not([disabled]),
.formulario input[type='button']:hover:not([disabled]),
.formulario input[type='reset']:hover:not([disabled]),
.boton a:hover:not([disabled]) {

	box-shadow: 2px 2px 2px #0008,	inset 1px 1px 2px #eee8;
	 filter:brightness(1.5);
 }


.boton:not(.conjunto):active,
.formulario input[type='submit']:active,
.boton a:active {
	box-shadow: inset 1px 1px 3px #000;
	}

") ?>


<?=css ("


.mensaje {
	background-color: #ffd;
	color: #c85;
	border-width: 1px;
	border-style: solid;

	border-radius: 0.25em;
}

") ?>




