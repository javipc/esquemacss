


/*  S V G */

.icono,
.panel *:first-child svg,
.panel *:first-child img,
.menu nav a img,
.menu nav a svg,
.boton:not(.conjunto) img,
.boton a img,
.boton:not(.conjunto) svg,
.boton a svg {	
	margin : 0px 0.5em 0px 0px;
	height: 1.5em;
	width: 1.5em;
	display: inline-block;
	vertical-align: middle;
}
