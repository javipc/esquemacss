

img { max-width: 100%;}
header { font-size: larger;}



h1, h2, h3, h4, h5, h6, .h {	
	margin : <?=$css_h_margen; ?>;
	padding: <?=$css_h_relleno; ?>;
}






/* divisores de contenidos */

/*
	Contenidos
*/


.encabezado {
	width  : 100%;	
	margin : <?=$css_encabezado_margen; ?>;
	padding: <?=$css_encabezado_relleno; ?>;
}




.encabezado h1,
.encabezado h2 {
	vertical-align: middle;
	margin: 2px
}

.encabezado .icono img,
.encabezado .icono svg {
	height: 3em;
	vertical-align: middle;	
}


.encabezado img+h1,
.encabezado svg+h1 {
	display: inline-block;
}







/* Divide secciones en la página */

.cuerpo,
.seccion {	
	line-height : 1.5;	
	margin : <?=$css_seccion_margen; ?>;
	padding: <?=$css_seccion_relleno; ?>;
	
}



/* Divide el contenido en una sección */

.panel,
.contenido,
.bloque {	
	margin : <?=$css_bloque_margen; ?>;
	padding: <?=$css_bloque_relleno; ?>;
}



.articulo {
  text-align: justify;  
  	margin : <?=$css_articulo_margen; ?>;
	padding: <?=$css_articulo_relleno; ?>;
}




/* Panel */

.panel {

}


.panel > *:first-child {
  font-weight:bold;
}



.panel > * {	
	margin : <?=$css_panel_interior_margen; ?>;
	padding: <?=$css_panel_interior_relleno; ?>;
}

.panel > *:not(:first-child) {


}





.pie {	
	margin : <?=$css_pie_margen; ?>;
	padding: <?=$css_pie_relleno; ?>;	
}

.pie svg {
	height: 1.25em;
	vertical-align: middle;
}

.pie > p {    
	margin : <?=$css_pie_p_margen; ?>;
	padding: <?=$css_pie_p_relleno; ?>;
}





/* espaciado básico */

.cita,
.texto,
.codigo,
.mensaje {
	margin : <?=$css_texto_margen; ?>;
	padding: <?=$css_texto_relleno; ?>;
}

.formulario input[type=button],
.formulario input[type=reset],
.formulario input[type=submit],
.boton:not(.conjunto),
.boton a {
	margin : <?=$css_boton_margen; ?>;
	padding: <?=$css_boton_relleno; ?>;
}

.formulario input:not([type=checkbox]):not([type=button]):not([type=submit]):not([type=reset]),
.formulario select,
.formulario textarea
{
	
	margin : <?=$css_formulario_campo_margen; ?>;
	padding: <?=$css_formulario_campo_relleno; ?>;
}

.formulario input[type=checkbox]{
	margin : <?=$css_formulario_campo_margen; ?>;
}


/* --------------------  */








.formulario input:not([type=checkbox]),
.formulario select,
.formulario textarea,
.mensaje,
.boton:not(.conjunto),
.boton a  {
	display: inline-block;
	vertical-align: middle;
	font-family: 'Ubuntu-L';
	line-height:1em;
	text-decoration: none;
}



