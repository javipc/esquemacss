

/*
   Diseño Móvil
*/


<?=css ("

@media (max-width: 750px) {


	.horizontal:not(.siempre) > *,
	.fila:not(.siempre),
	.columna:not(.siempre),
	.tabla:not(table):not(.conjunto):not(.siempre),
	.tabla:not(table):not(.conjunto):not(.siempre) > *,
	.tabla:not(table):not(.conjunto):not(.siempre) > * > *
	{	display: block;	}

	.ocultable  { display: none;}

	.movil_centrado_unico > *,
	.movil_centrado  { text-align: center;}
	.movil_izquierda { text-align: left;  }
	.movil_derecha   { text-align: right; }

	.derecho   { right:  .1em;}
	.superior  { top:    .1em;}
	.izquierdo { left:   .1em;}
	.inferior  { bottom: .1em;}


	.seccion,
	.cuerpo     { padding  : 1vw;	}
	.encabezado {	font-size: 2vw; }




	h1, h2, h3, h4, h5, h6 {	padding: .1em 0px;	}



	h1 { font-size: 1.25rem ;}
	h2 { font-size: 1.2rem  ;}
	h3 { font-size: 1.15rem ;}
	h4 { font-size: 1.1rem  ;}
	h5 { font-size: 1.05rem ;}
	h6 { font-size: 1rem    ;}

	.menu.desplegable.fijo:active {


	}
	
	
	.ventana {
		top: 0px;
		left: 0px;
		right:0px;
		bottom: 0px;
		border-radius: 0px;
	  }

	  
}

") ?>
