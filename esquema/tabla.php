

/*
	Etiqueta TABLE
*/


<?php

	echo css ("



.conjunto.tabla table,
table.tabla { width: 100%;}

.conjunto.tabla table th,
table.tabla th {
  color: #fff;
  background-color: #777;
  padding: 1em;
    border-radius: 0.25em;
}

.conjunto.tabla table td,
table.tabla td { padding: 1em;}

.conjunto.tabla table tr:not(:first-child):nth-child(odd),
table.tabla tr:not(:first-child):nth-child(odd) { background-color: #ddd;}

.conjunto.tabla table tr:not(:first-child):nth-child(even),
table.tabla tr:not(:first-child):nth-child(even) { background-color: #eee; }

.conjunto.tabla table tr:not(:first-child):hover,
table.tabla tr:not(:first-child):hover      { background-color: #fff; }

") ?>






/*
	Organización de contenidos (Grillas)
*/



.tabla:not(table):not(.conjunto) {
	display: table;
	width  : 100%;
	border-spacing: 1vw;
}



.fila,
.tabla:not(table):not(.conjunto) > *
{	display: table-row; }


.columna,
.tabla:not(table):not(.conjunto) > * > *
 {
	display: table-cell;
	vertical-align: top;
	padding: 1vw;
	margin:  1vw;
}




.tabla:not(table):not(.conjunto).anchofijo,
.horizontal.anchofijo { table-layout:fixed;}


/* elementos horizontales */


.horizontal  {
	display:table ;
	width: 100%;
	border-spacing: .25em;
}

.horizontal > * { display: table-cell;vertical-align: top;	}




/*	Elemetos verticales */
.menu nav:not(.horizontal),
.vertical nav {	display: inline-block;}

.menu nav:not(.horizontal) a,
.vertical a {	display: block;	}

