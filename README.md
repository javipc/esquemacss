# Esquema CSS

 Todos los sitios se ven iguales, parecen copias. De hecho lo son, ya que nadie diseña. La opción favorita es que el usuario realice una conexión a un sitio externo (vulnerando la privacidad) para obtener un CSS enorme y repleto de selectores que nunca se utilizarán. Y lo peor, usar javascript para armar el estilo de la página.

Es sabido que javascript se utiliza únicamente para mejorar la experiencia del usuario. El sitio debe funcionar perfectamente sin él. Entonces si un sitio depende de javascript para funcionar (o peor, para generar el CSS) está definitivamente mal hecho.

Esquema CSS proviene de diferentes entornos, tomando lo mejor y lo que sirve de cada uno.

<img src="esquemacss.png" alt="Captura de Pantalla" height="400">

## Logros

Esquema CSS utiliza unidades relativas, dejando de lado los pixeles (a diferencia de los más famosos entornos CSS). Al usar medidas relativas se ahorra código css y se adapta a diferentes pantallas y tamaños de fuentes de forma automática. 

Esquema CSS reutiliza el código al máximo posible.
Este tipo de clases no lo vas a ver:
 * btn-light
 * btn-dark
 * btn-primary
 * btn-secondary
 * btn-success
 * btn-outline-primary
 * btn-outline-danger
 * btn-outline-dark
 * btn-outline-light
 * btn-outline-success
 * ... y la lista sigue.

En esquema CSS no existen los prefijos "boton" porque las clases (por ejemplo colores) se adaptan automáticamente a través de los selectores.


## Características:
* Sin javascript (usar javascript en el diseño es una MALA PRÁCTICA).
* No usa FLEX, lo que permite una mayor compatibilidad.
* Reutilizable ("boton azul", "boton verde", "codigo" - Reutiliza "boton").
* Asignación rápida de diseño a múltiples componentes (Clase CONJUNTO) - Evita el código repetitivo y deja el HTML más limpio.
* Agrupación de dimensiones según contexto, para personalizar rápidamente el estilo.
* Columnas flexibles e inflexibles que se ajustan automáticamente (No se especifica la cantidad de columnas).
* Tablas automáticas, solo se declara el contenedor "TABLA", las filas y columnas se detectan automáticamente.
* Unidades relativas, permite adaptar a múltiples tamaños de pantallas de forma inteligente.
* Versión móvil.

## Contacto
Consultas, elogios, puteadas: http://javicel.t.me/

## ¿Cómo colaborar?:
https://gitlab.com/javipc/mas

Gracias por la visita 👋🏿
